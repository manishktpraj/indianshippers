import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServicesProvider } from '../providers/services/services';
import { HttpModule } from '@angular/http';
import {IonicStorageModule} from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Contacts } from '@ionic-native/contacts';
  

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment
} from '@ionic-native/google-maps';

import { AfterSplashPage } from '../pages/after-splash/after-splash';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { ConfirmBookingPage } from '../pages/confirm-booking/confirm-booking';
import { EditprofilePage } from '../pages/editprofile/editprofile';
import { FinishBookingPage } from '../pages/finish-booking/finish-booking';
import { ForgetWithOtpPage } from '../pages/forget-with-otp/forget-with-otp';
import { HelpSupportPage } from '../pages/help-support/help-support';
import { LoadingPage } from '../pages/loading/loading';
import { LoginMobilePage } from '../pages/login-mobile/login-mobile';
import { LoginWithOtpPage } from '../pages/login-with-otp/login-with-otp';
import { LoginWithPasswordPage } from '../pages/login-with-password/login-with-password';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { MytripPage } from '../pages/mytrip/mytrip';
import { MywalletPage } from '../pages/mywallet/mywallet';
import { OffersPage } from '../pages/offers/offers';
import { PaymentPage } from '../pages/payment/payment';
import { RateCardPage } from '../pages/rate-card/rate-card';
import { ReferEarnPage } from '../pages/refer-earn/refer-earn';
import { RegistrationDetailPage } from '../pages/registration-detail/registration-detail';
import { SetDropLocationPage } from '../pages/set-drop-location/set-drop-location';
import { StartTripPage } from '../pages/start-trip/start-trip';
import { TripDetailPage } from '../pages/trip-detail/trip-detail';
import { UnloadingPage } from '../pages/unloading/unloading';
import { SelectbookvehiclePage } from '../pages/selectbookvehicle/selectbookvehicle';
import { FeedbackPage } from '../pages/feedback/feedback';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { FaqPage } from '../pages/faq/faq';
import { GoodstypePage } from '../pages/goodstype/goodstype';
import { ContactlistPage } from '../pages/contactlist/contactlist';
import { WaitconfirmPage } from '../pages/waitconfirm/waitconfirm';
import { TrackscreenPage } from '../pages/trackscreen/trackscreen';






@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,AfterSplashPage,ChangePasswordPage,ConfirmBookingPage,EditprofilePage,FinishBookingPage,
ForgetWithOtpPage,HelpSupportPage,LoadingPage,LoginMobilePage,LoginWithOtpPage,LoginWithPasswordPage,MyaccountPage
,MytripPage,MywalletPage,OffersPage,PaymentPage,RateCardPage,ReferEarnPage,RegistrationDetailPage,SetDropLocationPage,StartTripPage,TripDetailPage,UnloadingPage,SelectbookvehiclePage,FeedbackPage,AboutusPage,FaqPage,GoodstypePage,ContactlistPage,WaitconfirmPage,TrackscreenPage
  ],
  imports: [
    BrowserModule,
	HttpModule,
    IonicModule.forRoot(MyApp),
	IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,AfterSplashPage,ChangePasswordPage,ConfirmBookingPage,EditprofilePage,FinishBookingPage,
ForgetWithOtpPage,HelpSupportPage,LoadingPage,LoginMobilePage,LoginWithOtpPage,LoginWithPasswordPage,MyaccountPage
,MytripPage,MywalletPage,OffersPage,PaymentPage,RateCardPage,ReferEarnPage,RegistrationDetailPage,SetDropLocationPage,StartTripPage,TripDetailPage,UnloadingPage,SelectbookvehiclePage,FeedbackPage,AboutusPage,FaqPage,GoodstypePage,ContactlistPage,WaitconfirmPage,TrackscreenPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
	Device,
	Diagnostic,
	Geolocation,
	GoogleMaps,
	Environment,
	SocialSharing,
	InAppBrowser,
	Contacts,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicesProvider
  ]
})
export class AppModule {}
