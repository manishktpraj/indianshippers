import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AfterSplashPage } from '../pages/after-splash/after-splash';
import { MywalletPage } from '../pages/mywallet/mywallet';
import { ReferEarnPage } from '../pages/refer-earn/refer-earn';
import { HelpSupportPage } from '../pages/help-support/help-support';
import { MytripPage } from '../pages/mytrip/mytrip';
import { RateCardPage } from '../pages/rate-card/rate-card';
import { PaymentPage } from '../pages/payment/payment';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { FeedbackPage } from '../pages/feedback/feedback';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { ServicesProvider } from '../providers/services/services';
import * as firebase from 'firebase/app';

const config = {
 // apiKey: 'YOUR_APIKEY',
  apiKey: 'AIzaSyBnJD_V_OZZpm33pBpqsksh4ljdsuRuytw',
  //authDomain: 'YOUR_AUTH_DOMAIN',
  authDomain: 'indian-shipper-partner.firebaseapp.com',
  databaseURL: 'https://indian-shipper-partner.firebaseio.com',
  projectId: 'indian-shipper-partner',
  storageBucket: 'YOUR_STORAGE_BUCKET',
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
 
///  rootPage: any = PaymentPage;
  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;
user_info:any={user_first_name:''};
  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public menu:MenuController,public inapp:InAppBrowser,private services:ServicesProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];
this.services.getuser().then((user) => {
	if(user!=null)
	{
this.user_info = user;
	}
});
       firebase.initializeApp(config);


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  openwallet() {
	  this.menu.close();
    this.nav.push(MywalletPage);
  }
  openrefer() {
	  this.menu.close();
    this.nav.push(ReferEarnPage);
  }
  opensupport() {
	  this.menu.close();
    this.nav.push(HelpSupportPage);
  }
  openmytrip() {
	  this.menu.close();
    this.nav.push(MytripPage);
  }
  openratecard() {
	  this.menu.close();
    this.nav.push(RateCardPage);
  }
  openpayment() {
	  this.menu.close();
    this.nav.push(PaymentPage);
  }
  openmyaccount() {
	  this.menu.close();
    this.nav.push(MyaccountPage);
  }
  rateus()
  {
	  this.inapp.create('https://play.google.com/store/apps/details?id=codexo.indianshippers');
  }
  aboutus()
  {
	  this.menu.close();
	   this.nav.push(AboutusPage);
	  
  }
  feedback()
  {
	  this.menu.close();
	  this.nav.push(FeedbackPage);
	  
  }
}
