import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoodstypePage } from './goodstype';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(GoodstypePage),
  ],
})
export class GoodstypePageModule {}
