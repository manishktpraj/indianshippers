import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the GoodstypePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-goodstype',
  templateUrl: 'goodstype.html',
})
export class GoodstypePage {
category_list:any=[];
response:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,public viewCtrl:ViewController) {
	  
	  this.goodscategory();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoodstypePage');
  }
  
  
   goodscategory()
  {
	  let  loading =   this.services._loader_content();	
    loading.present();
	  let credentials:any={category_id:0,distance:0};
	this.services.getgoodscategory(credentials).subscribe((result)=>{  
	 this.category_list =result.goodscategory;
	 this.response = result;
 
    },(error)=>{
		
	},()=>{
		loading.dismiss();
    });
  }
  selectcategory(cat)
  {
	  this.viewCtrl.dismiss({selected_cat:cat,response: this.response});
  }
  closeoffer()
  {
	  this.viewCtrl.dismiss({});
	  
  }
}
