import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import { LoginMobilePage } from '../login-mobile/login-mobile';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the AfterSplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-after-splash',
  templateUrl: 'after-splash.html',
})
export class AfterSplashPage {
user_info:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,private menu:MenuController,private statusBar:StatusBar) {
	  statusBar.hide();
	  this.menu.enable(false);
	  this.services.getuser().then((user) => {
this.user_info = user;
if(user!=null)
{ 
 this.navCtrl.setRoot(HomePage); 
}
});

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AfterSplashPage');
  }
openloginmobile(){
	this.navCtrl.push(LoginMobilePage);
}
}
