import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
faq_list:any=[];
shownGroup = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,private services:ServicesProvider) {
	  this.getfaq();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }
getfaq(){	
	let loading =this.services._loader_content();
	loading.present();
	let data ={};
	this.services.getfaq(data).subscribe((result)=>{  
	this.faq_list = 	result.result; 	

    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
}
toggleGroup(group) {
    if (this.isGroupShown(group)) {
        this.shownGroup = null;
    } else {
        this.shownGroup = group;
    }
};
isGroupShown(group) {
    return this.shownGroup === group;
};
}
