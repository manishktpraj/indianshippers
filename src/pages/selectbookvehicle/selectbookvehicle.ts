import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConfirmBookingPage } from '../confirm-booking/confirm-booking';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the SelectbookvehiclePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
import * as moment from 'moment';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
@IonicPage()
@Component({
  selector: 'page-selectbookvehicle',
  templateUrl: 'selectbookvehicle.html',
})
export class SelectbookvehiclePage {
map: any;
droppoint:any={};
user_location_info:any={pickuplocation:'',droplocation:''};
category_list:any=[];
showspinner:boolean;
public fireAuth: any;
driver_online_list:any=[];
driver_online:any;
markers: any = [];
selected_vh_id:any=0;
selected_vh_obj:any={};
avail_driver_by_category:any=[];
intervaldata:any;
distance:any=0;

 
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
 	
this.fireAuth = firebase.auth();
this.driver_online = firebase.database().ref('/online_drivers');
let u:any={pickuplocation:{},droplocation:{}};
this.services.getuserlocation().then((user) => {
u = user;
this.user_location_info = u.pickuplocation;
this.droppoint = u.droplocation;
this.distance = this.services.getDistanceFromLatLonInKm(this.user_location_info.lat,this.user_location_info.lng,this.droppoint.lat,this.droppoint.lng);
this.getcategory();
});
	
  }
  ionViewWillLeave()
  {
	    clearInterval(this.intervaldata);
  }
   ionViewWillEnter()
   {
if(this.droppoint.lat!=undefined)
{
this.getcurrentdriver();	
}
   }

  loadMap(){
	   var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        let latLng = new google.maps.LatLng(this.user_location_info.lat, this.user_location_info.lng);
		        let latLngend = new google.maps.LatLng(this.droppoint.lat, this.droppoint.lng);

        let mapOption = {
          center: latLng,
          zoom: 10,
		   disableDefaultUI: true
	  } 
        let element = document.getElementById('maps'); 
        this.map = new google.maps.Map(element, mapOption); 
       
             
            // directionsDisplay.setOptions({polylineOptions: polyline});
        // directionsDisplay.setMap(this.map);
         this.calculateAndDisplayRoute(directionsService, directionsDisplay,latLng,latLngend);
      }
	  
calculateAndDisplayRoute(directionsService, directionsDisplay,start,end)
{
	let	 icons = {
        start: {url:'assets/icon/bluedot.jpg',scaledSize: new google.maps.Size(10, 10)},
        end: {url:'assets/icon/bluecircle.png',scaledSize: new google.maps.Size(10, 10)}
    };
	let obj=this;
        directionsService.route({
          origin: start,
          destination:end,
          travelMode: 'DRIVING',
		     unitSystem: google.maps.UnitSystem.METRIC
        }, function(response, status) {
          if (status === 'OK') {
          //  directionsDisplay.setDirections(response);
		    directionsDisplay = new google.maps.DirectionsRenderer(
  {
			map: obj.map,
			directions: response,
			suppressMarkers: true,
			polylineOptions: {
			strokeColor: '#29166f',
			strokeOpacity: 0.9,
			strokeWeight: 2
    }
 
  });
			 let leg = response.routes[0].legs[0];
			 console.log(response, 'tester');
               obj.makeMarker(leg.start_location, icons.start, "title", obj.map);
                 obj.makeMarker(leg.end_location, icons.end, 'title', obj.map);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
	
//	this.addmarker();	
	
      }
	  
	     makeMarker(position, icon, title, map) {
       let m = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
		 m.setMap(map);
    }

  
  getcategory()
  {
	  	   this.showspinner =true;

	  let credentials:any={};
	this.services.getcategorydata(credentials).subscribe((result)=>{  
	 this.category_list =result.category;
	if(this.category_list.length>0)
	{
		let data:any=[];
		this.category_list.forEach((Iten)=>{
			let avil:any=[];
			avil = this.vehiclebycategory(Iten.category_id);
			Iten['lengthvh'] = avil;
			Iten['mintime'] = this.services.getmintimeavaildriver(avil);
			Iten['price'] = this.services.getpriceondistance(Iten,this.distance);
			data.push(Iten);
			if(this.selected_vh_id<=0 && avil.length>0)
			{
			 
			 this.selected_vh_id =Iten.category_id;
			 this.avail_driver_by_category = avil;
			 this.selected_vh_obj = Iten;
			 
			}
		});
		this.category_list = data;
	}
	 setTimeout(()=>{ this.showspinner =false;this.loadMap();},1000);
	   
    },(error)=>{
		
	},()=>{
    });
  }
  
  getcurrentdriver()
{
  	 firebase.database().ref('/online_drivers').on('value',snapshot => {
		 
         this.driver_online_list = [];
 		let t:any={};
		let distance:any=0;
           Object.keys(snapshot.val()).forEach(key => {
			 t=snapshot.val()[key];
			distance = this.services.getDistanceFromLatLonInKm(this.user_location_info.lat,this.user_location_info.lng,t.lat,t.lng);
			 if(t.lat!=undefined && t.onlineStatus==1 && t.vehicleType>0 && distance<=3  && t.bookingCurrent=='{}')
			 {
				 t['distance'] = distance;
				 t['timed'] = this.services.gettimecalculate(distance);
		  this.driver_online_list.push(t);   
		 
			 }  
        });
 
      });
	this.intervaldata = setInterval(()=>{
//	this.addmarker();	
	},5000)   
}

   setMapOnAll() {
        for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(null);
        }
      }
addmarker()
{
	
	if(this.category_list.length>0)
	{
		let data:any=[];
		this.category_list.forEach((Iten)=>{
			let avil:any=[];
			avil = this.vehiclebycategory(Iten.category_id);
			Iten['lengthvh'] = avil;
		 	Iten['mintime'] = this.services.getmintimeavaildriver(avil);

			data.push(Iten);
			if(this.selected_vh_id<=0 && avil.length>0)
			{
			 
			 this.selected_vh_id =Iten.category_id;
			 this.avail_driver_by_category = avil;
			 this.selected_vh_obj = Iten;
			 
			}
		});
		this.category_list = data;
	}
	
	this.setMapOnAll();
 	this.markers =[];
	let latLng:any;
	let i:any=0;
	
	let ic= {31:'http://indianshippers.com/img/truck-light.png',32:'http://indianshippers.com/img/heavy-truck.png',33:'http://indianshippers.com/img/trucking.png'}
 this.services.log(this.driver_online_list,'list driver');
	this.driver_online_list.forEach((Item)=>{
		
		if(Item.vehicleType==this.selected_vh_id)
		{
		    latLng  = new google.maps.LatLng(Item.lat, Item.lng);
     
	this.markers.push(new google.maps.Marker({
          position: latLng,
          title: Item.driverId,
		  draggable: false,
          icon:{url:ic[Item.vehicleType],scaledSize: new google.maps.Size(30, 30)}
        }));	
		// this.markers[i].setMap(this.map);
		// this.markers[i].setMap(this.map);
	this.markers[i].setMap(this.map);
    
		i++;
		}
	});
}
  
  
  vehiclebycategory(strvhid)
  {
	  let lengtharray =[];
	this.driver_online_list.forEach((Item)=>{
		if(Item.vehicleType==strvhid)
		{
			lengtharray.push(Item);
		}
	});  
	return lengtharray;
  }
  
  setviehicletype(cat)
  {
	  if(this.vehiclebycategory(cat.category_id).length>0)
	  {
	this.selected_vh_id = cat.category_id; 
	this.avail_driver_by_category = this.vehiclebycategory(cat.category_id);
	 this.selected_vh_obj =cat;
	  }else{
		  
		  this.services.presentToast('No '+cat.category_name+' Vehicle available');
	  }
  }
  
  opencbooking() {
	  if(this.selected_vh_id>0)
	  {
		  	    clearInterval(this.intervaldata);

    this.navCtrl.push(ConfirmBookingPage,{selected_vh_obj:this.selected_vh_obj,avail_driver_by_category:this.avail_driver_by_category});
	  }else{
				  this.services.presentToast('No Vehicle available at this location please change pickup location');
 	  
	  }
  }


}
