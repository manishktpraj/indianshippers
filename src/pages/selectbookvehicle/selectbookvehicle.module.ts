import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectbookvehiclePage } from './selectbookvehicle';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(SelectbookvehiclePage),
  ],
})
export class SelectbookvehiclePageModule {}
