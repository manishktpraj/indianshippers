import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginWithOtpPage } from './login-with-otp';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(LoginWithOtpPage),
  ],
})
export class LoginWithOtpPageModule {}
