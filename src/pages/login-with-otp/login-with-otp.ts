import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController } from 'ionic-angular';
import { RegistrationDetailPage } from '../registration-detail/registration-detail';
import { Device } from '@ionic-native/device';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';
import { ForgetWithOtpPage } from '../forget-with-otp/forget-with-otp';

/**
 * Generated class for the LoginWithOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window: any;

@IonicPage()
@Component({
  selector: 'page-login-with-otp',
  templateUrl: 'login-with-otp.html',
})
export class LoginWithOtpPage {
user_otp:any='';
user_id:any=0;
user_tocken_id:any='';
user_device_id:any='';
user_mobile:any='';
login_error:any='';
form_page:any='';
 constructor(public navCtrl: NavController, public navParams: NavParams,private device:Device,private services:ServicesProvider,private menu:MenuController) {
	 	  this.menu.enable(false);

	 this.user_mobile =this.navParams.get('user_phone');
	  this.user_id =this.navParams.get('user_id');
	  let objElement: any = this;
    if(objElement.device.platform=='Android')
    {
      window.FirebasePlugin.onTokenRefresh(function(token) {
        objElement.user_tocken_id = token;
        objElement.user_device_id = objElement.device.uuid;
      }, function(error) {
        console.error(error);
      });
    }
	if(this.navParams.get('fromd')!=undefined)
	{
		this.form_page = this.navParams.get('fromd');
	}
	if(this.form_page=='fromloginwithotp')
	{
		this.resendotp();		
	}
	if(this.form_page=='forgot')
	{
		this.resendotp();		
	}
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginWithOtpPage');
  }
openregistration(){
	if(this.user_otp=='')
	{
		this.login_error='Please Enter Password';
		return;
	}
if(this.user_otp.length!=4)
	{	
this.login_error='Please Enter 4 digit otp code';
return;
	}
		let loading =this.services._loader_content();
	loading.present();
		let data ={user_otp:this.user_otp,user_id:this.user_id,user_tocken_id:this.user_tocken_id,user_device_id:this.user_device_id};
    this.services.onVerifyRegisterOtp(data).subscribe((result)=>{  
	if(result.message=='ok')
	{
	//this.services.saveuser(result.results);
	if(this.form_page=='forgot')
	{
		  this.navCtrl.push(ForgetWithOtpPage,{user_id:this.user_id,user_mobile:this.user_mobile,user_tocken_id:this.user_tocken_id,user_device_id:this.user_device_id});	
	}else{
		if(result.results.user_status==1)
		{
			this.services.saveuser(result.results);
			this.navCtrl.setRoot(HomePage);
		}else{
		    this.navCtrl.push(RegistrationDetailPage,{user_id:this.user_id,user_mobile:this.user_mobile});	
		}	
	}
	}else{
     this.login_error= result.notification;	
	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	
	///this.navCtrl.push(RegistrationDetailPage);
}
resendotp()
{
	  
	let loading =this.services._loader_content();
	loading.present();	
	let data ={user_id:this.user_id};
	this.services.onResendOtp(data).subscribe((result)=>{  
	
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });

}
}
