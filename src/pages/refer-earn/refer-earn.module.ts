import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferEarnPage } from './refer-earn';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ReferEarnPage),
  ],
})
export class ReferEarnPageModule {}
