import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ReferEarnPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-refer-earn',
  templateUrl: 'refer-earn.html',
})
export class ReferEarnPage {
user_info:any={};
result_data:any={user_share_code:''};
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,private socialSharing: SocialSharing) {
this.services.getuser().then((user) => {
this.user_info = user;
this.getShareScreen();
});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferEarnPage');
  }
getShareScreen(){	
	let loading =this.services._loader_content();
	loading.present();
	let data ={user_id:this.user_info.user_id};
	this.services.getShareScreendata(data).subscribe((result)=>{  
	this.result_data = result;
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
}
shareApp()
{
	let loading =this.services._loader_content();
	loading.present();
      this.socialSharing.share(this.result_data.text_share_message, 'Indian Shippers').then(() => {
          // Sharing via email is possible
           loading.dismiss();
        }).catch(() => {
          // Sharing via email is not possible
           loading.dismiss();
        });

}
}
