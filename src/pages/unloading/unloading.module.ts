import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UnloadingPage } from './unloading';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(UnloadingPage),
  ],
})
export class UnloadingPageModule {}
