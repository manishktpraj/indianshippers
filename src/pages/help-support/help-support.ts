import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FaqPage } from '../faq/faq';

/**
 * Generated class for the HelpSupportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-help-support',
  templateUrl: 'help-support.html',
})
export class HelpSupportPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpSupportPage');
  }
onShowFaq()
{
	this.navCtrl.push(FaqPage);
	
}
}
