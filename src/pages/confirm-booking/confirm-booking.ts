import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { GoodstypePage } from '../goodstype/goodstype';
import { OffersPage } from '../offers/offers';
import { Contacts, ContactFieldType, ContactFindOptions } from '@ionic-native/contacts';
import { ContactlistPage } from '../contactlist/contactlist';
import { WaitconfirmPage } from '../waitconfirm/waitconfirm';
import { PaymentPage } from '../payment/payment';

/**
 * Generated class for the ConfirmBookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
import * as moment from 'moment';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
@IonicPage()
@Component({
  selector: 'page-confirm-booking',
  templateUrl: 'confirm-booking.html',
})
export class ConfirmBookingPage {
map: any;
category:any={};
goods_type:any={};
avail_driver:any=[];
driver_online_list:any=[];
public fireAuth: any; 
driver_online:any;
user_location_info:any={};
droppoint:any={};
markers: any = [];
public allContacts: any
user_info:any={};
distance:any=0;
eta_price:any=0;
choose_method:any='cash';

marker:any=null;
marker_2:any=null;
marker_3:any=null;
marker_4:any=null;
marker_5:any=null;
marker_6:any=null;
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,public modalCtrl:ModalController,private contacts:Contacts) {
	  this.category = this.navParams.get('selected_vh_obj');
	  this.avail_driver = this.navParams.get('avail_driver_by_category');
	    this.fireAuth = firebase.auth();
    this.driver_online = firebase.database().ref('/online_drivers');
 	  this.services.getuser().then((user) => {
this.user_info = user;
});
  let u:any={pickuplocation:{},droplocation:{}};
		 this.services.getuserlocation().then((user) => {
		 u = user;
this.user_location_info = u.pickuplocation;
this.droppoint = u.droplocation;
this.distance = this.services.getDistanceFromLatLonInKm(this.user_location_info.lat,this.user_location_info.lng,this.droppoint.lat,this.droppoint.lng);
this.eta_price = this.services.getpriceondistance(this.category,this.distance);
this.services.log(this.droppoint,"this.droppoint");
this.getcurrentdriver();
this.loadMap(); 
});  
  }
ionViewWillEnter()
   {
	   
   }
    ionViewWillLeave()
  {
	    //clearInterval(this.intervaldata);
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmBookingPage');
  }
opengoods()
{
	 // console.log(this.cart_grandtotal);
    let profileModal = this.modalCtrl.create(GoodstypePage);
	profileModal.onDidDismiss(data => {
	if(data.selected_cat!=null)
	{
		this.goods_type = data.selected_cat;
	}		
    });
      profileModal.present(); 
}
opencoupon()
{
	let profileModal = this.modalCtrl.create(OffersPage,{eta_price:this.eta_price,user_id:this.user_info.user_id,fromcart:'fromcart'});
	profileModal.onDidDismiss(data => {	
    });
      profileModal.present(); 
}
  getcurrentdriver()
{
  	 firebase.database().ref('/online_drivers').on('value',snapshot => {
         this.driver_online_list = [];
 		let t:any={};
           Object.keys(snapshot.val()).forEach(key => {
			 t=snapshot.val()[key];
			 if(t.lat!=undefined && t.onlineStatus==1  && this.services.getDistanceFromLatLonInKm(this.user_location_info.lat,this.user_location_info.lng,t.lat,t.lng)<=3  && t.bookingCurrent=='{}' && t.vehicleType==this.category.category_id)
			 {	 
		  this.driver_online_list.push(t);     
			 }  
        });
      });
	
}
  loadMap(){
	   var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        let latLng = new google.maps.LatLng(this.user_location_info.lat, this.user_location_info.lng);
        let latLngend = new google.maps.LatLng(this.droppoint.lat, this.droppoint.lng);

        let mapOption = {
          center: latLng,
          zoom: 10,
		   disableDefaultUI: true
	  } 
        let element = document.getElementById('mapss'); 
        this.map = new google.maps.Map(element, mapOption); 
       
             
           //   directionsDisplay.setOptions({polylineOptions: polyline});
          directionsDisplay.setMap(this.map);
         this.calculateAndDisplayRoute(directionsService, directionsDisplay,latLng,latLngend);
      }
	  
	    calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
		let	 icons = {
        start: {url:'http://indianshippers.com/img/bluedot.jpg',scaledSize: new google.maps.Size(10, 10)},
        end: {url:'http://indianshippers.com/img/bluecircle.png',scaledSize: new google.maps.Size(10, 10)}
    };
	let obj=this;
        directionsService.route({
          origin: start,
          destination:end,
          travelMode: 'DRIVING',
		     unitSystem: google.maps.UnitSystem.METRIC
        }, function(response, status) {
          if (status === 'OK') {
          //  directionsDisplay.setDirections(response);
		    directionsDisplay = new google.maps.DirectionsRenderer(
  {
                    map: obj.map,
                    directions: response,
                    suppressMarkers: true,
					    polylineOptions: {
       strokeColor: '#29166f',
             strokeOpacity: 0.9,
             strokeWeight: 2
    }
 
  });
			 let leg = response.routes[0].legs[0];
			 console.log(response, 'tester');
               obj.makeMarker(leg.start_location, icons.start, "title", obj.map);
                 obj.makeMarker(leg.end_location, icons.end, 'title', obj.map);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
	
	this.addmarker();	
	
      }
	  
	     makeMarker(position, icon, title, map) {
       let m = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
		 m.setMap(map);
    }
   setMapOnAll() {
        for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(null);
        }
      }
addmarker()
{
  	this.markers =[];
	let latLng:any;
	let i:any=0;
	let ic= {31:'http://indianshippers.com/img/truck-light.png',32:'http://indianshippers.com/img/heavy-truck.png',33:'http://indianshippers.com/img/trucking.png'}
 this.services.log(this.driver_online_list,'list driver');
		let obj=this;
	if(this.marker!=null && this.driver_online_list[0]!=undefined)
		{
	let counter = 0;
	let c=0;
let interval = window.setInterval(function() { 
   counter++;
  c= counter / 10000;
    	latLng  = new google.maps.LatLng(obj.driver_online_list[0].lat, obj.driver_online_list[0].lng+c );
	obj.marker.setPosition(latLng);
  if (counter >= 10) {
    window.clearInterval(interval);   
  }
}, 10);
		}else if(obj.driver_online_list[0]!=undefined)
		{
			let objdata =this.driver_online_list[0];
		latLng  = new google.maps.LatLng(obj.driver_online_list[0].lat, obj.driver_online_list[0].lng);
	this.marker=new google.maps.Marker({
	position: latLng,
	title: this.driver_online_list[0].driverId,
	draggable: false,
	icon:{url:ic[objdata.vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker.setMap(this.map);	
		}
		if(this.marker_2!=null && obj.driver_online_list[1]!=undefined)
		{
let counter = 0;
//ssthis.marker_2.setPosition(latLng);
let c=0;
let interval2 = window.setInterval(function() { 
  counter++;
  c = counter / 10000;
  	latLng  = new google.maps.LatLng(obj.driver_online_list[1].lat, obj.driver_online_list[1].lng+c );
	obj.marker_2.setPosition(latLng);
  if (counter >= 10) {
    window.clearInterval(interval2);   
  }
}, 10);
 		}else if(obj.driver_online_list[1]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[1].lat, obj.driver_online_list[1].lng);
	this.marker_2=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[1].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[1].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_2.setMap(this.map);
		}
	if( this.marker_3!=null && obj.driver_online_list[2]!=undefined)
		{
	    latLng  = new google.maps.LatLng(obj.driver_online_list[2].lat, obj.driver_online_list[2].lng);
	  	this.marker_3.setPosition(latLng);

 		}else if(obj.driver_online_list[2]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[2].lat, obj.driver_online_list[2].lng);
	this.marker_3=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[2].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[2].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_3.setMap(this.map);
		}
		if(this.marker_4!=null && obj.driver_online_list[3]!=undefined)
		{
	latLng  = new google.maps.LatLng(obj.driver_online_list[3].lat, obj.driver_online_list[3].lng);
		  	this.marker_4.setPosition(latLng);

 		}else if(obj.driver_online_list[3]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[3].lat, obj.driver_online_list[3].lng);
	this.marker_4=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[3].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[3].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_4.setMap(this.map);
		}
	
if(this.marker_5!=null && obj.driver_online_list[4]!=undefined)
		{
	latLng  = new google.maps.LatLng(obj.driver_online_list[4].lat, obj.driver_online_list[4].lng);
 		  	this.marker_5.setPosition(latLng);
 		}else if(obj.driver_online_list[4]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[4].lat, obj.driver_online_list[4].lng);
	this.marker_5=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[4].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[4].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_5.setMap(this.map);
		}
	 

}
 selectcontact()
 {
	this.contacts.find(['displayName', 'name', 'phoneNumbers'], {filter: "", multiple: true})
    .then(data => {
      this.allContacts = data
	  this.navCtrl.push(ContactlistPage,{contact:this.allContacts})
	  this.services.log(this.allContacts,"this.allContacts");
    });
 }
 confirmbooking()
 {
	 let driverid =this.services.getdriveridbycomma(this.driver_online_list);
	let data = {user_id:this.user_info.user_id,category_id:this.category.category_id,gcategory_id:this.goods_type.gcategory_id,distance:this.distance,booking_payment_method:'cash',booking_helper_count:0,booking_drop_contact_name:this.user_info.user_first_name+' '+this.user_info.user_last_name,booking_drop_contact_no:this.user_info.user_mobile_number,pickup_lat:this.user_location_info.lat,pickup_long:this.user_location_info.lng,pickup_address:this.user_location_info.fullAddress,drop_lat:this.droppoint.lat,drop_long:this.droppoint.lng,drop_address:this.droppoint.fullAddress,driver_id:driverid,booking_coupon_id:0,booking_coupon_title:''};
	 	this.navCtrl.push(WaitconfirmPage,{data:data})

	
 }
 openpayment()
 {
	let profileModal = this.modalCtrl.create(PaymentPage,{choose:this.choose_method,fromcart:'fromcart'});
	profileModal.onDidDismiss(data => {
	 		this.choose_method = data.choosepaymentmethod;
    });
      profileModal.present();  
 }
}
