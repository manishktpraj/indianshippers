import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { ServicesProvider } from '../../providers/services/services';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the RegistrationDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window: any;

@IonicPage()
@Component({
  selector: 'page-registration-detail',
  templateUrl: 'registration-detail.html',
})
export class RegistrationDetailPage {
user_id:any='';
user_mobile:any ='';
login_error:any='';
validations_form: FormGroup;
user_device_id:any='';
user_tocken_id:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public services:ServicesProvider,private device:Device,private menu:MenuController) {
	   this.menu.enable(false);
	 this.user_id =  this.navParams.get('user_id');
	 this.user_mobile =  this.navParams.get('user_mobile');
	 this.validations_form = this.formBuilder.group({
user_first_name: new FormControl(''),
user_last_name: new FormControl(''),
user_email: new FormControl(''),
user_mobile_number: new FormControl(this.user_mobile),
user_password: new FormControl('')
});
	  let objElement: any = this;
    if(objElement.device.platform=='Android')
    {
      window.FirebasePlugin.onTokenRefresh(function(token) {
        objElement.user_tocken_id = token;
        objElement.user_device_id = objElement.device.uuid;
      }, function(error) {
        console.error(error);
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationDetailPage');
  }
processregister()
{
let credentials =this.validations_form.value;   
if(credentials['user_first_name']=='')
{
		  this.login_error = 'Please Enter First Name';
		  return;
}  
if(credentials['user_last_name']=='')
{
		  this.login_error = 'Please Enter Last Name';
		  return;
}  
if(credentials['user_email']=='')
{
		  this.login_error = 'Please Enter Email';
		  return;
} 
if(!this.services.ValidateEmail(credentials['user_email']))
{
		  this.login_error = 'Please Enter Valid Email';
		  return;
} 
if(credentials['user_mobile_number']=='')
{
		  this.login_error = 'Please Enter Mobile Number';
		  return;
}  
if(credentials['user_password']=='')
{
		  this.login_error = 'Please Enter Password';
		  return;
}  	
  credentials['user_id'] =this.user_id;
  credentials['user_tocken_id'] =this.user_tocken_id;
  credentials['user_device_id'] =this.user_device_id;
		let loading =this.services._loader_content();
	loading.present();
     this.services.onFinalRegistration(credentials).subscribe((result)=>{  
	if(result.message=='ok')
	{
	 this.services.saveuser(result.results);
	/// this.services.saveuser(result.results);
       this.navCtrl.setRoot(HomePage);
	}else{
     this.login_error= result.notification;	
	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	
}
}
