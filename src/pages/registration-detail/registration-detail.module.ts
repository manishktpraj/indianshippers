import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistrationDetailPage } from './registration-detail';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(RegistrationDetailPage),
  ],
})
export class RegistrationDetailPageModule {}
