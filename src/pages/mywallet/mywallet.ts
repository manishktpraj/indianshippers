import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the MywalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mywallet',
  templateUrl: 'mywallet.html',
})
export class MywalletPage {
user_info:any={};
user_balance:any=0.00;
history_data:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
this.services.getuser().then((user) => {
this.user_info = user;
this.openwallethistory();
});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MywalletPage');
  }
openwallethistory(){	
 
	let loading =this.services._loader_content();
	loading.present();
	
	
	let data ={user_id:this.user_info.user_id};
	this.services.getwallethistorynew(data).subscribe((result)=>{  
	
	this.user_balance = result.user_balance;
	this.history_data = result.results;
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	// this.getd();
}

}
