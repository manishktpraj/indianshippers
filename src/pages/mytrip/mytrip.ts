import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { TripDetailPage } from '../trip-detail/trip-detail';

/**
 * Generated class for the MytripPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-mytrip',
  templateUrl: 'mytrip.html',
})
export class MytripPage {
user_info:any={};
history_data:any=[];
ongoing:any=[];
activetav:any='Ongoing';
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
	  this.services.getuser().then((user) => {
this.user_info = user;
this.getTrip();
});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MytripPage');
  }
  settab(activetab)
  {
	this.activetav =   activetab;
  }
getTrip(){	
 
	let loading =this.services._loader_content();
	loading.present();
 	
	
	let data ={user_id:this.user_info.user_id};
	this.services.gettriphistory(data).subscribe((result)=>{  
	
//	this.user_balance = result.user_balance;
 this.history_data = result.past;
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	// this.getd();
}
getdataformat(datae)
{
	let data = moment(datae).format('LLLL');
	return data;
}
opendetail(booking)
{
	this.navCtrl.push(TripDetailPage,{booking:booking});
}

}
