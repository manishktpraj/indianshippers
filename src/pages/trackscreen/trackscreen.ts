import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the TrackscreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;
import * as moment from 'moment';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
@IonicPage()
@Component({
  selector: 'page-trackscreen',
  templateUrl: 'trackscreen.html',
})
export class TrackscreenPage {
current_booking:any={};
map: any;
user_location_info:any={pickuplocation:'',droplocation:''};
maupdated:any=0;
startmarker:any;
endmarker:any;
fireAuth:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
	  this.current_booking = this.navParams.get('current_booking');
	  this.services.log(this.current_booking,"this.current_booking");
	  this.fireAuth = firebase.auth();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrackscreenPage');
  }
  ionViewWillEnter()
  {
	// this.maupdated=0;
	 this.getsnap();
  }
getsnap()
{
	let driver:any={};
	let refered = firebase.database().ref('/online_drivers').child(this.current_booking.driver_id);
	refered.on('value', snapshot => {
//console.log('snapshot.val()', snapshot.val(),' snapshot.val()');
let datra = snapshot.val();
if(datra!="{}")
	{
	 driver =datra;
	 this.current_booking =JSON.parse(driver.bookingCurrent);
	 		console.log(this.current_booking,"datra");

	if(this.current_booking.booking_status==2)
	{
	 this.loadMap(driver.lat,driver.lng,this.current_booking.booking_from_lat,this.current_booking.booking_from_long);
	}else{
		 
			 this.loadMap(driver.lat,driver.lng,this.current_booking.booking_to_lat,this.current_booking.booking_to_long);
	} 
///	console.log(this.current_booking,"this.current_booking"); 
///	console.log(this.current_booking['booking_id'],"this.current_booking driver"); 
	}
	
});
}
  loadMap(startlat,startlong,enlat,endlong){
	  console.log(startlat,startlong,enlat,endlong);
	  let latLngend:any;
	  if(this.maupdated==0)
	  {
		  this.maupdated =1;
	   var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;

        let latLng = new google.maps.LatLng(startlat, startlong);
		      latLngend   = new google.maps.LatLng(enlat,endlong);

        let mapOption = {
          center: latLng,
          zoom: 10,
		   disableDefaultUI: true
	  } 
        let element = document.getElementById('maps_route'); 
        this.map = new google.maps.Map(element, mapOption); 
       
        
         // directionsDisplay.setOptions({polylineOptions: polyline});
        // directionsDisplay.setMap(this.map);
         this.calculateAndDisplayRoute(directionsService, directionsDisplay,latLng,latLngend);
	  }else{
		  
		 		          latLngend = new google.maps.LatLng(enlat,endlong);
 
		  this.startmarker.setPosition(latLngend);
		  
	  }
      }
	  
	    calculateAndDisplayRoute(directionsService, directionsDisplay,start,end) {
		let	 icons = {
        start: {url:'assets/icon/heavy-truck.png',scaledSize: new google.maps.Size(25, 25)},
        end: {url:'http://indianshippers.com/img/bluecircle.png',scaledSize: new google.maps.Size(10, 10)}
    };
	let obj=this;
        directionsService.route({
          origin: start,
          destination:end,
          travelMode: 'DRIVING',
		     unitSystem: google.maps.UnitSystem.METRIC
        }, function(response, status) {
          if (status === 'OK') {
          //  directionsDisplay.setDirections(response);
		    directionsDisplay = new google.maps.DirectionsRenderer(
  {
                    map: obj.map,
                    directions: response,
                    suppressMarkers: true,
					    polylineOptions: {
       strokeColor: '#29166f',
             strokeOpacity: 0.9,
             strokeWeight: 2
    }
 
  });
			 let leg = response.routes[0].legs[0];
			 console.log(response, 'tester');
               obj.makeMarker(leg.start_location, icons.start, "title", obj.map,1);
                 obj.makeMarker(leg.end_location, icons.end, 'title', obj.map,2);
          } else {
         //   window.alert('Directions request failed due to ' + status);
          }
        });
	
 	
      }
	  
	     makeMarker(position, icon, title, map,type) {
			 if(type==1)
			 {
       this.startmarker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
		 this.startmarker.setMap(map);
			 }else{
			 this.endmarker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
		 this.endmarker.setMap(map);	 
			 }
    }

}
