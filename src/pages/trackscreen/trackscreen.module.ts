import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackscreenPage } from './trackscreen';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(TrackscreenPage),
  ],
})
export class TrackscreenPageModule {}
