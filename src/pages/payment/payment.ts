import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
choosepaymentmethod:any='cash';
from:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,private viewCtrl:ViewController) 
  {
	  if(this.navParams.get('choose')!=undefined)
	  {
	  this.choosepaymentmethod = this.navParams.get('choose');
	  }
	  if(this.navParams.get('fromcart')!=undefined)
	  {
		  this.from = this.navParams.get('fromcart');
	  }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

savebycheck(strtype)
{
	if(this.from!='')
	{
	this.choosepaymentmethod = strtype;
	this.closeoffer();
	}
}
closeoffer()
{
	this.viewCtrl.dismiss({choosepaymentmethod:this.choosepaymentmethod})
	
}
}
