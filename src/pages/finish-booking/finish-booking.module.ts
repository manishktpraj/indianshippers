import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishBookingPage } from './finish-booking';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(FinishBookingPage),
  ],
})
export class FinishBookingPageModule {}
