import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetDropLocationPage } from './set-drop-location';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(SetDropLocationPage),
  ],
})
export class SetDropLocationPageModule {}
