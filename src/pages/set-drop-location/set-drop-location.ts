import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SelectbookvehiclePage } from '../selectbookvehicle/selectbookvehicle';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the SetDropLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-set-drop-location',
  templateUrl: 'set-drop-location.html',
})
export class SetDropLocationPage {
	 @ViewChild('myInput') myInput;
user_location_info:any={fullAddress:''};
  GoogleAutocomplete: any;
autocompleteItems: any = [];
  autocomplete: any;
  autocomplete_pickup: any;
  geocoder: any;
stractivelocation:any='drop';
recentsearch:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
	   this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
	  this.autocomplete = { input: '' };
	  this.autocomplete_pickup = { input: '' };
	  this.autocompleteItems = [];
	  this.geocoder = new google.maps.Geocoder();

  }
setType(statype)
{
this.stractivelocation =statype; 
this.services.log(this.stractivelocation,"this.stractivelocation");
}
   ionViewWillEnter()
  {  
  let u:any={pickuplocation:{}};
  this.services.getuserlocation().then((user) => {
	  u =user;
this.user_location_info = u.pickuplocation;
 this.autocomplete_pickup.input = this.user_location_info.fullAddress;
this.services.log(this.user_location_info,'set-drop-location');
});
this.services.getuserrecentlocation().then((loc) => {
	if(loc!=null)
	{
	this.recentsearch = loc;
	this.autocompleteItems = loc;
	}
	});
	
	  
  }
   
  ionViewDidLoad() {
    console.log('ionViewDidLoad SetDropLocationPage');
  }
openchoosevehicle(bcategorys)
{
	 if(this.stractivelocation=='pickup')
	 {
  let place_data: any = {'placeId': bcategorys.place_id};
  this.autocomplete_pickup.input = bcategorys.description;
  this.geocodeUserAddress(place_data);
  this.autocompleteItems = [];

	 }else{
	let place_data: any = {'placeId': bcategorys.place_id};
  this.autocomplete.input = bcategorys.description;
  this.geocodeUserAddress(place_data);	 
	 }
	
	 if(this.checkifplaceidexist(bcategorys)<=0)
	 {
		  this.recentsearch.push(bcategorys);
         this.services.saverecentsearch(this.recentsearch);
	 }
}
checkifplaceidexist(bcategorys)
{
	console.log(bcategorys,"bcategorys");
	let count:any=0;
	this.recentsearch.forEach((key)=>{
		if(key.id==bcategorys.id)
		{
			count++;
		}
	});
	
	return count;
}
updateSearchResults(){
	 if(this.autocomplete_pickup.input!='')
	 {
  if (this.autocomplete.input == '' ) {
    this.autocompleteItems = [];
    return;
  }
  this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
	(predictions, status) => {
    this.autocompleteItems = [];
	if(predictions!=null)
	{
	predictions.forEach((prediction) => {
	//	this.services.log(prediction);
        this.autocompleteItems.push(prediction);
      });
	}
  });
	 }else{
		this.services.presentToast('Please Select Pickup location'); 
	 }
}
 updateSearchResultsPickup(){
	
  if (this.autocomplete_pickup.input=='') {
    this.autocompleteItems = [];
    return;
  }
  this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete_pickup.input },
	(predictions, status) => {
    this.autocompleteItems = [];
	if(predictions!=null)
	{
	predictions.forEach((prediction) => {
	//	this.services.log(prediction);
        this.autocompleteItems.push(prediction);
      });
	}
  });
	  
}
 
 placeToAddress(place){
       var address: any = {};
       place.address_components.forEach(function(c) {
			let types: any = c.types; 
			let lastType: any = types[types.length-1];
			address[types[0]] = c;
			address[lastType] = c;
           switch(c.types[0]){
                case 'street_number':
                    address.StreetNumber = c;
                    break;
                case 'route':
                    address.StreetName = c;
                    break;
                case 'neighborhood': case 'locality':    // North Hollywood or Los Angeles?
                    address.City = c;
                    break;
                case 'administrative_area_level_1':     //  Note some countries don't have states
                    address.State = c;
                    break;
                case 'postal_code':
                    address.Zip = c;
                    break;
                case 'country':
                    address.Country = c;
                    break;
               
            }
        });
	 
		if(place.fullAddress!=null && place.fullAddress!=undefined )
		{
		address['address']= place.fullAddress;	
		}
		if(place.formatted_address!=null && place.formatted_address!=undefined )
		{
		address['address']= place.formatted_address;	
		}
        return address;
    } 

   geocodeUserAddress(addressSource: any) {
	let  loading =   this.services._loader_content();	
    loading.present();
	 this.geocoder.geocode(addressSource, (results, status) => {
		
    if(status === 'OK' && results[0]){
      let position = {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
      };
	  let address: any = this.placeToAddress(results[0]);
	  let droplocation :any={};
 	  droplocation = {address: address['address'], fullAddress: results[0].formatted_address, lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()};
		 /// this.services.saveuserlocation(this.userLocationInfo);
		  if(this.stractivelocation=='pickup')
	 {
		 					   loading.dismiss();
		  		  this.services.saveuserlocation({pickuplocation:droplocation,droplocation:{}});
				this.user_location_info = droplocation;  
				  this.myInput.setFocus();
				  this.stractivelocation ='drop';
	 }else{
		 this.services.saveuserlocation({pickuplocation:this.user_location_info,droplocation:droplocation});
				  setTimeout(()=>{
					   loading.dismiss();
			this.navCtrl.push(SelectbookvehiclePage);
			  
				  },1000); 
	 }
 
    }
  });
    }


}
