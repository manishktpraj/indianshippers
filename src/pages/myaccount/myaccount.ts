import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { AfterSplashPage } from '../after-splash/after-splash';
import { MywalletPage } from '../mywallet/mywallet';
import { ReferEarnPage } from '../refer-earn/refer-earn';
import { HelpSupportPage } from '../help-support/help-support';
import { MytripPage } from '../mytrip/mytrip';
import { PaymentPage } from '../payment/payment';
import { OffersPage } from '../offers/offers';
import { EditprofilePage } from '../editprofile/editprofile';

/**
 * Generated class for the MyaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {
user_info:any={user_first_name:''};
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,public alertCtrl:AlertController) {
	  this.services.getuser().then((user) => {
this.user_info = user;
console.log(this.user_info);
});
  }
  ionViewWillEnter()
  {   this.services.getuser().then((user) => {
this.user_info = user;
console.log(this.user_info);
});
	  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
  }

  
  
  openwallet()
  {
	  this.navCtrl.push(MywalletPage);
  }
  openorder()
  {
	  	  this.navCtrl.push(MytripPage);

  }
  openpayment()
  {
	 	  	  this.navCtrl.push(PaymentPage);
 
  }
  openreferral()
  {
	  	 	  	  this.navCtrl.push(ReferEarnPage);

  }
  openoffers()
  {
	  
		  	 	  	  this.navCtrl.push(OffersPage);
  
  }
  openhelpsupport()
  {
	  		  	 	  	  this.navCtrl.push(HelpSupportPage);

  }
  
  logoutUser()
  {
	

let  alert =    this.alertCtrl.create({
title: 'Are you sure?',
message: 'Do you want to logout?',
buttons: [
{
text: 'Cancel',
role: 'cancel',
handler: () => {

}
},
{
text: 'Yes',
handler: () => {
this.services.saveuser(null);
this.navCtrl.setRoot(AfterSplashPage);
}
}
]
});
 alert.present();
  
  }
editprofile()
{
	this.navCtrl.push(EditprofilePage);
	
}
}
