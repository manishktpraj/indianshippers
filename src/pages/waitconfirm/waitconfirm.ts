import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';

/**
 * Generated class for the WaitconfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-waitconfirm',
  templateUrl: 'waitconfirm.html',
})
export class WaitconfirmPage {
data:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
	  this.data = this.navParams.get('data');
  }

  ionViewDidLoad() {
	  	this.findnearbydriver();
    console.log('ionViewDidLoad WaitconfirmPage');
	 
  }
  
  findnearbydriver()
  {
	  let loading =this.services._loader_content();
	loading.present();
 	this.services.confirmbooking(this.data).subscribe((result)=>{  
 	 if(result.message=='ok')
	 {
		 		this.navCtrl.setRoot(HomePage);
	 }
    },(error)=>{
		
	},()=>{
			   loading.dismiss();

    });
  }

}
