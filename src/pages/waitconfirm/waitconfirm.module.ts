import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WaitconfirmPage } from './waitconfirm';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(WaitconfirmPage),
  ],
})
export class WaitconfirmPageModule {}
