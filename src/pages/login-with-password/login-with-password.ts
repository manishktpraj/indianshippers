import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { LoginWithOtpPage } from '../login-with-otp/login-with-otp';
import { Device } from '@ionic-native/device';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';

declare var window: any;
/**
 * Generated class for the LoginWithPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login-with-password',
  templateUrl: 'login-with-password.html',
})
export class LoginWithPasswordPage {
user_mobile:any='';
user_id:any ='';
user_device_id:any ='';
user_tocken_id:any ='';
user_password:any ='';
login_error:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,private device:Device,private services:ServicesProvider,private menu:MenuController) {
	    this.menu.enable(false);

	  this.user_mobile =this.navParams.get('user_phone');
	  this.user_id =this.navParams.get('user_id');
	  let objElement: any = this;
    if(objElement.device.platform=='Android')
    {
      window.FirebasePlugin.onTokenRefresh(function(token) {
        objElement.user_tocken_id = token;
        objElement.user_device_id = objElement.device.uuid;
      }, function(error) {
        console.error(error);
      });
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginWithPasswordPage');
  }
loginWithPassword(){	
	if(this.user_password=='')
	{
		this.login_error='Please Enter Password';
		return;
	}
	 
	let loading =this.services._loader_content();
	loading.present();
	this.login_error ='';
	
	
	let data ={user_password:this.user_password,user_id:this.user_id,user_tocken_id:this.user_tocken_id,user_device_id:this.user_device_id};
	this.services.onLoginPassword(data).subscribe((result)=>{  
	if(result.message=='ok')
	{
	this.services.saveuser(result.results);
    this.navCtrl.setRoot(HomePage);	
	}else{
     this.login_error= result.notification;	
		//this.navCtrl.push(LoginWithOtpPage);

	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	// this.getd();
}
  
openotp(){
	this.navCtrl.push(LoginWithOtpPage,{user_phone:this.user_mobile,user_id:this.user_id,fromd:'fromloginwithotp'});
}
forgot()
{
this.navCtrl.push(LoginWithOtpPage,{user_phone:this.user_mobile,user_id:this.user_id,fromd:'forgot'});	
}
backtosplash()
{
	this.navCtrl.pop();	
}
}
