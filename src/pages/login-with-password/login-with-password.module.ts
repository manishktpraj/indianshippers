import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginWithPasswordPage } from './login-with-password';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(LoginWithPasswordPage),
  ],
})
export class LoginWithPasswordPageModule {}
