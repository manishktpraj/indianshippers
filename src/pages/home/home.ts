import { Component } from '@angular/core';
import { NavController ,ViewController,NavParams,MenuController,AlertController} from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';
import { ServicesProvider } from '../../providers/services/services';
import { SetDropLocationPage } from '../set-drop-location/set-drop-location';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AfterSplashPage } from '../after-splash/after-splash';
import { TrackscreenPage } from '../trackscreen/trackscreen';
import {  StatusBar } from '@ionic-native/status-bar';


import * as moment from 'moment';

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';

declare var google: any;
@Component({
selector: 'page-home',
templateUrl: 'home.html'
})
export class HomePage {
GoogleAutocomplete: any;
geocoder: any;
markers: any = [];
map: any;
userLocationInfo:any={};
current_marker:any;
public fireAuth: any;
driver_online_list:any=[];
driver_online:any;
current_lat:any='';
current_long:any='';
intervaldata:any;
user_info:any={};
result_data:any={};
current_booking:any={};


marker:any=null;
marker_2:any=null;
marker_3:any=null;
marker_4:any=null;
marker_5:any=null;
marker_6:any=null;


constructor(public navCtrl: NavController, public navParams: NavParams,private geolocation: Geolocation, private diagnostic: Diagnostic,public services:ServicesProvider,public viewCtrl:ViewController,private menu:MenuController,public alertCtrl:AlertController,public socialSharing:SocialSharing,private statusBar:StatusBar) {
	this.statusBar.hide();
this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
this.geocoder = new google.maps.Geocoder();
this.markers = [];
this.fireAuth = firebase.auth();
this.driver_online = firebase.database().ref('/online_drivers');

}

ionViewWillEnter()
{
this.menu.enable(true);
///	 this.loadMap(26.854866,75.824295);
/// this.tryGeolocation();
 this.services.getuser().then((user) => {
	if(user!=null)
	{
this.user_info = user;
this.getDashboard();
 ///this.tryGeolocation();

this.tryGeolocationAfterPermission();

	}else{
		this.navCtrl.setRoot(AfterSplashPage);
	}
});
}
ionViewWillLeave()
{
 clearInterval(this.intervaldata);
}
getDashboard()
{
//let loading =this.services._loader_content();
///	loading.present();
	let data ={user_id:this.user_info.user_id};
	this.services.getDashboardData(data).subscribe((result)=>{  
	if(result.message=='ok')
	{
		this.current_booking = result.current_booking;
 		this.getsnap();
 	}
     },(error)=>{
		
	},()=>{
   ///   loading.dismiss();
    });	
}
getsnap()
{
	if(this.current_booking.driver_id!=undefined)
	{
	let refered = firebase.database().ref('/online_drivers').child(this.current_booking.driver_id).child("bookingCurrent");
	refered.on('value', snapshot => {
console.log('snapshot.val()', snapshot.val(),' snapshot.val()');
let datra = snapshot.val();
if(datra!="{}")
	{
      this.current_booking =JSON.parse(datra) ;
	//console.log(this.current_booking,"this.current_booking"); 
	//console.log(this.current_booking['booking_id'],"this.current_booking driver"); 
	} 
	
});
	}

}
tryGeolocation() {
	this.diagnostic.getPermissionAuthorizationStatus(this.diagnostic.permission.ACCESS_FINE_LOCATION).then((status) => {
	//console.log("AuthorizationStatus");
	//console.log(status);
	if (status != this.diagnostic.permissionStatus.GRANTED) {
	this.diagnostic.requestRuntimePermission(this.diagnostic.permission.ACCESS_FINE_LOCATION).then((data) => {
	console.log("getCameraAuthorizationStatus");
	console.log(data);
	if(data == this.diagnostic.permissionStatus.GRANTED) {
	this.diagnostic.isLocationEnabled().then((isEnabled) => {
	if(!isEnabled){
	//handle confirmation window code here and then call switchToLocationSettings
	this.diagnostic.switchToLocationSettings();
	this.tryGeolocation();
	}else{
	this.tryGeolocationAfterPermission();
	}});
	}
	})
	} else {
	this.diagnostic.isLocationEnabled().then((isEnabled) => {
	if(!isEnabled){
	//handle confirmation window code here and then call switchToLocationSettings
	this.diagnostic.switchToLocationSettings();
	this.tryGeolocation();
	}else{
	this.tryGeolocationAfterPermission();
	}});
	// this.tryGeolocationAfterPermission();
	}
	}, (statusError) => {
	this.loadMap(26.854866,75.824295);
	this.tryGeolocationAfterPermission();
	console.log("statusError");
	console.log(statusError);
	});
} 
tryGeolocationAfterPermission(){
	 let optionsGPS = {timeout: 20000, enableHighAccuracy: true};
	this.geolocation.getCurrentPosition(optionsGPS).then((resp) => {
	this.loadMap(resp.coords.latitude,resp.coords.longitude);
	let pos = {
	lat: resp.coords.latitude,
	lng: resp.coords.longitude
	};
	this.current_lat = resp.coords.latitude;
	this.current_long = resp.coords.longitude;
	let place_data: any = {'location': pos};
	this.geocodeUserAddress(place_data);
	this.getcurrentdriver();
	}).catch((error) => {
	});

	let watch = this.geolocation.watchPosition();
	let ob=this;
	watch.subscribe((data) => {
	if(ob.current_marker!=undefined)
	{
	let posnew = {
	lat: data.coords.latitude,
	lng: data.coords.longitude
	};
	ob.current_lat = data.coords.latitude;
	ob.current_long = data.coords.longitude;
	ob.current_marker.setPosition(posnew);
	}
	});
} 


getcurrentdriver()
{
	firebase.database().ref('/online_drivers').on('value',snapshot => {

	this.driver_online_list = [];
	let t:any={};
	Object.keys(snapshot.val()).forEach(key => {
	t=snapshot.val()[key];
	if(t.lat!=undefined && t.onlineStatus==1 && t.vehicleType>0 && this.services.getDistanceFromLatLonInKm(this.current_lat,this.current_long,t.lat,t.lng)<=3 && t.bookingCurrent=='{}')
	{
	this.driver_online_list.push(t);   
	}  
	});

	});
	setTimeout(()=>{
		this.updatemarker();
	},4000)
		

	this.intervaldata = setInterval(()=>{
	this.updatemarker();	
	},30000);  
}


 
 
updatemarker()
{
	 
	//this.setMapOnAll();
    ///this.markers =[];
	let latLng:any;
	let i:any=0;
	let ic= {31:'http://indianshippers.com/img/heavy-truck.png',32:'http://indianshippers.com/img/heavy-truck.png',33:'http://indianshippers.com/img/heavy-truck.png'};
	this.services.log(this.driver_online_list,'list driver');
	let obj=this;
	if(this.marker!=null && this.driver_online_list[0]!=undefined)
		{
	let counter = 0;
	let c=0;
let interval = window.setInterval(function() { 
   counter++;
  c= counter / 10000;
    	latLng  = new google.maps.LatLng(obj.driver_online_list[0].lat, obj.driver_online_list[0].lng+c );
	obj.marker.setPosition(latLng);
  if (counter >= 10) {
    window.clearInterval(interval);   
  }
}, 10);
		}else if(obj.driver_online_list[0]!=undefined)
		{
			let objdata =this.driver_online_list[0];
		latLng  = new google.maps.LatLng(obj.driver_online_list[0].lat, obj.driver_online_list[0].lng);
	this.marker=new google.maps.Marker({
	position: latLng,
	title: this.driver_online_list[0].driverId,
	draggable: false,
	icon:{url:ic[objdata.vehicleType],scaledSize: new google.maps.Size(25, 24)}
	});	
	this.marker.setMap(this.map);	
		}
		if(this.marker_2!=null && obj.driver_online_list[1]!=undefined)
		{
let counter = 0;
//ssthis.marker_2.setPosition(latLng);
let c=0;
let interval2 = window.setInterval(function() { 
  counter++;
  c = counter / 10000;
  	latLng  = new google.maps.LatLng(obj.driver_online_list[1].lat, obj.driver_online_list[1].lng+c );
	obj.marker_2.setPosition(latLng);
  if (counter >= 10) {
    window.clearInterval(interval2);   
  }
}, 10);
 		}else if(obj.driver_online_list[1]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[1].lat, obj.driver_online_list[1].lng);
	this.marker_2=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[1].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[1].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_2.setMap(this.map);
		}
	if( this.marker_3!=null && obj.driver_online_list[2]!=undefined)
		{
	    latLng  = new google.maps.LatLng(obj.driver_online_list[2].lat, obj.driver_online_list[2].lng);
	  	this.marker_3.setPosition(latLng);

 		}else if(obj.driver_online_list[2]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[2].lat, obj.driver_online_list[2].lng);
	this.marker_3=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[2].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[2].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_3.setMap(this.map);
		}
		if(this.marker_4!=null && obj.driver_online_list[3]!=undefined)
		{
	latLng  = new google.maps.LatLng(obj.driver_online_list[3].lat, obj.driver_online_list[3].lng);
		  	this.marker_4.setPosition(latLng);

 		}else if(obj.driver_online_list[3]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[3].lat, obj.driver_online_list[3].lng);
	this.marker_4=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[3].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[3].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_4.setMap(this.map);
		}
	
if(this.marker_5!=null && obj.driver_online_list[4]!=undefined)
		{
	latLng  = new google.maps.LatLng(obj.driver_online_list[4].lat, obj.driver_online_list[4].lng);
 		  	this.marker_5.setPosition(latLng);
 		}else if(obj.driver_online_list[4]!=undefined){
	latLng  = new google.maps.LatLng(obj.driver_online_list[4].lat, obj.driver_online_list[4].lng);
	this.marker_5=new google.maps.Marker({
	position: latLng,
	title: obj.driver_online_list[4].driverId,
	draggable: false,
	icon:{url:ic[obj.driver_online_list[4].vehicleType],scaledSize: new google.maps.Size(25, 25)}
	});	
	this.marker_5.setMap(this.map);
		}
	 

	
}

loadMap(lat, lng){
	let latLng = new google.maps.LatLng(lat, lng);
	let mapOption = {
	center: latLng,
	zoom: 17,
	mapTypeId:'roadmap',
	disableDefaultUI: true
	} 
	let element = document.getElementById('map'); 
	this.map = new google.maps.Map(element, mapOption); 
	this.current_marker  = new google.maps.Marker({
	position: latLng,
	title: 'Current Location',
	draggable: true,
	icon:{url:'assets/icon/mv2.gif',scaledSize: new google.maps.Size(40, 40)}
	});
	this.current_marker.setMap(this.map);
}

placeToAddress(place){
	var address: any = {};
	place.address_components.forEach(function(c) {
	let types: any = c.types; 
	let lastType: any = types[types.length-1];
	address[types[0]] = c;
	address[lastType] = c;
	switch(c.types[0]){
	case 'street_number':
	address.StreetNumber = c;
	break;
	case 'route':
	address.StreetName = c;
	break;
	case 'neighborhood': case 'locality':    // North Hollywood or Los Angeles?
	address.City = c;
	break;
	case 'administrative_area_level_1':     //  Note some countries don't have states
	address.State = c;
	break;
	case 'postal_code':
	address.Zip = c;
	break;
	case 'country':
	address.Country = c;
	break;

	}
	});

	if(place.fullAddress!=null && place.fullAddress!=undefined )
	{
	address['address']= place.fullAddress;	
	}
	if(place.formatted_address!=null && place.formatted_address!=undefined )
	{
	address['address']= place.formatted_address;	
	}
	return address;
} 

geocodeUserAddress(addressSource: any) {
	let  loading =   this.services._loader_content();	
	loading.present();
	this.geocoder.geocode(addressSource, (results, status) => {
	loading.dismiss();
	if(status === 'OK' && results[0]){
	let position = {
	lat: results[0].geometry.location.lat,
	lng: results[0].geometry.location.lng
	};
	let address: any = this.placeToAddress(results[0]);
	this.userLocationInfo = {address: address['address'], fullAddress: results[0].formatted_address, lat: results[0].geometry.location.lat(),
	lng: results[0].geometry.location.lng()};
	this.services.saveuserlocation({pickuplocation:this.userLocationInfo,droplocation:{}});
	}
	});
}

currentpostison()
{
	this.map.setCenter(new google.maps.LatLng(this.current_lat, this.current_long));
}
openchoosedrop()
{
	this.navCtrl.push(SetDropLocationPage);
}
setMapOnAll() {
	for (var i = 0; i < this.markers.length; i++) {
	this.markers[i].setMap(null);
	}
}
cancelbooking()
{
	let alert = this.alertCtrl.create({
    title: 'Confirm purchase',
    message: 'Do you want to cancel?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.cancelbookingRequest();
        }
      }
    ]
  });
  alert.present();
}
cancelbookingRequest()
{
 let loading =this.services._loader_content();
 	loading.present();
	let data ={user_id:this.user_info.user_id,booking_id:this.current_booking.booking_id};
	this.services.cancelbookingrequest(data).subscribe((result)=>{  
	if(result.message=='ok')
	{
			///	this.current_booking = result.current_booking;
this.current_booking  ={};
let c = result.current_booking;
		 firebase.database().ref('/online_drivers').child(c.driver_id).child("bookingCurrent").set("{}");
		this.services.simplealert('Canceled','Your Booking Canceled');
	}
     },(error)=>{
		
	},()=>{
     loading.dismiss();
    });	
}
sharedetail()
{
	let loading =this.services._loader_content();
	loading.present();
      this.socialSharing.share('', 'Indian Shippers').then(() => {
          // Sharing via email is possible
           loading.dismiss();
        }).catch(() => {
          // Sharing via email is not possible
           loading.dismiss();
        });
	
}
opentrackscreen()
{
	this.navCtrl.push(TrackscreenPage,{current_booking:this.current_booking});
	
}
}
