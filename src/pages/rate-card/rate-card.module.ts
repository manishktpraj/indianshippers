import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RateCardPage } from './rate-card';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(RateCardPage),
  ],
})
export class RateCardPageModule {}
