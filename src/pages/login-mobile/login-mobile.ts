import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,MenuController} from 'ionic-angular';
import { LoginWithPasswordPage } from '../login-with-password/login-with-password';
import { ServicesProvider } from '../../providers/services/services';
import { LoginWithOtpPage } from '../login-with-otp/login-with-otp';

/**
 * Generated class for the LoginMobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-mobile',
  templateUrl: 'login-mobile.html',
})
export class LoginMobilePage {
mobile_number:any='';
login_error:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider,private menu:MenuController) {
	  this.menu.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginMobilePage');
  }
openlogin(){	
	if(this.mobile_number=='')
	{
		this.login_error='Please Enter Mobile Number';
		return;
	}
	if(!this.services.isValidMobile(this.mobile_number))
	{
		this.login_error='Please Enter Valid Mobile Number';
		return;
	}
	let loading =this.services._loader_content();
	loading.present();
	this.login_error ='';
	
	
	let data ={user_phone:this.mobile_number};
	this.services.onCheckRegister(data).subscribe((result)=>{  
	
	if(result.message=='ok')
	{
		if(result.status=='NEW')
		{
			this.navCtrl.push(LoginWithOtpPage,{user_id:result.user_id,user_phone:this.mobile_number});	
	
		}else{
	this.navCtrl.push(LoginWithPasswordPage,{user_id:result.user_id,user_phone:this.mobile_number});	
		}
	}else{
		
	this.login_error= 'Server Error Occur';	
	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	// this.getd();
}

backtosplash()
{
	this.navCtrl.pop();	
}
}
