import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginMobilePage } from './login-mobile';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(LoginMobilePage),
  ],
})
export class LoginMobilePageModule {}
