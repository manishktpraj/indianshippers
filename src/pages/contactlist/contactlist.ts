import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ContactlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactlist',
  templateUrl: 'contactlist.html',
})
export class ContactlistPage {
allcontact:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
	  this.allcontact =this.navParams.get('contact');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactlistPage');
  }

}
