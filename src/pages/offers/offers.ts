import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the OffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {
promocode_message:any='';
offerce:any=[];
eta_price:any=0;
user_id:any=0;
fromcart:boolean=false;
promocode:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl:ViewController,private services:ServicesProvider) {
	 this.eta_price = this.navParams.get('eta_price');
	 this.user_id = this.navParams.get('user_id');
	 if(this.navParams.get('fromcart'))
	 {
		 this.fromcart =true;
	 }
	  this.getofferce();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OffersPage');
  }
closeoffer()
{
	this.viewCtrl.dismiss();
}
getofferce(){	
	let loading =this.services._loader_content();
	loading.present();
	let data ={eta_price:this.eta_price,user_id:this.eta_price};
	this.services.getoffercedata(data).subscribe((result)=>{  
	this.offerce = 	result.result; 	

    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
}
applybyname()
{
	if(this.promocode=='')
	{
		this.promocode_message ='Please Enter Promocode';
		return false;
	}
	let loading =this.services._loader_content();
	loading.present();
	let data ={eta_price:this.eta_price,user_id:this.eta_price,promo_text:this.promocode};
	this.services.getoffercedata(data).subscribe((result)=>{  
       if(result.message=='ok')
	   {
		   
	   }else{
		   this.services.simplealert('Invalid','Please Enter Right Promocode')
	   }
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
}
applybyid()
{
	if(this.promocode=='')
	{
		this.promocode_message ='Please Enter Promocode';
		return false;
	}
	let loading =this.services._loader_content();
	loading.present();
	let data ={eta_price:this.eta_price,user_id:this.eta_price,promo_text:this.promocode};
	this.services.getoffercedata(data).subscribe((result)=>{  
       if(result.message=='ok')
	   {
		   
	   }else{
		   this.services.simplealert('Invalid','Please Enter Right Promocode')
	   }
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	
}
}
