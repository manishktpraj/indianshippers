import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';


/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
login_error:any='';
validations_form: FormGroup;
user_info:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder: FormBuilder,public services:ServicesProvider) {
	   this.validations_form = this.formBuilder.group({
user_first_name: new FormControl(''),
user_last_name: new FormControl(''),
user_email: new FormControl(''),
user_mobile_number: new FormControl('')
});
this.services.getuser().then((user) => {
this.user_info = user;
this.validations_form.patchValue({
user_first_name: this.user_info.user_first_name,
user_last_name: this.user_info.user_last_name,
user_email: this.user_info.user_email,
user_mobile_number:this.user_info.user_mobile_number,
});
});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }
processregister()
{
	let credentials =this.validations_form.value;   
if(credentials['user_first_name']=='')
{
		  this.login_error = 'Please Enter First Name';
		  return;
}  
if(credentials['user_last_name']=='')
{
		  this.login_error = 'Please Enter Last Name';
		  return;
}  
if(credentials['user_email']=='')
{
		  this.login_error = 'Please Enter Email';
		  return;
} 
if(!this.services.ValidateEmail(credentials['user_email']))
{
		  this.login_error = 'Please Enter Valid Email';
		  return;
} 
if(credentials['user_mobile_number']=='')
{
		  this.login_error = 'Please Enter Mobile Number';
		  return;
}  
  	credentials['user_phone'] =credentials['user_mobile_number'];
  credentials['user_id'] =this.user_info.user_id;
   
		let loading =this.services._loader_content();
	loading.present();
     this.services.updateprofile(credentials).subscribe((result)=>{  
	if(result.message=='ok')
	{
	 this.services.saveuser(result.results);
	/// this.services.saveuser(result.results);
        this.services.simplealert('Profile Updated','Your profile Updated Successfully.');
	}else{
     this.login_error= result.notification;	
	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
}
}
