import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { HomePage } from '../home/home';

/**
 * Generated class for the ForgetWithOtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forget-with-otp',
  templateUrl: 'forget-with-otp.html',
})
export class ForgetWithOtpPage {
user_password:any='';
user_conf_password:any='';
login_error:any='';
user_id:any=0;
user_tocken_id:any='';
user_device_id:any='';
  constructor(public navCtrl: NavController, public navParams: NavParams,public services:ServicesProvider) {
	  this.user_id =this.navParams.get('user_id');
	  this.user_tocken_id =this.navParams.get('user_tocken_id');
	  this.user_device_id =this.navParams.get('user_device_id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetWithOtpPage');
  }
  backtosplash()
  {
	  
  }
  forgot()
  {
	  if(this.user_password=='')
{
		  this.login_error = 'Please Enter Password';
		  return;
}  
if(this.user_conf_password=='')
{
		  this.login_error = 'Please Enter Confirm Password';
		  return;
}
if(this.user_conf_password!=this.user_password)
{
	 this.login_error = 'Confirm password and password not match';
		  return;
}
   let data = {user_id:this.user_id,user_password:this.user_password,user_tocken_id:this.user_tocken_id,user_device_id:this.user_device_id};
	let loading =this.services._loader_content();
	loading.present();
     this.services.updatepassword(data).subscribe((result)=>{  
	if(result.message=='ok')
	{
	  this.services.saveuser(result.results);
       this.navCtrl.setRoot(HomePage);
	}else{
     this.login_error= result.notification;	
	}
    },(error)=>{
		
	},()=>{
      loading.dismiss();
    });
	  
	  
  }

}
