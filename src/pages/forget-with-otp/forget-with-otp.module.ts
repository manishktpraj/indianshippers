import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForgetWithOtpPage } from './forget-with-otp';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ForgetWithOtpPage),
  ],
})
export class ForgetWithOtpPageModule {}
