import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * Generated class for the AboutusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-aboutus',
  templateUrl: 'aboutus.html',
})
export class AboutusPage {
my_url:any='http://indianshippers.com/about_us';
my_iframe_url:any='';

  constructor(public navCtrl: NavController, public navParams: NavParams,private sanitize: DomSanitizer) {
       this.my_iframe_url =  this.sanitize.bypassSecurityTrustResourceUrl(this.my_url);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutusPage');
  }

}
