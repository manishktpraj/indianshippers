import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { LoadingController,AlertController ,PopoverController,ActionSheetController,ToastController } from 'ionic-angular';
import {Storage} from '@ionic/storage';


/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServicesProvider {
    readonly YOUR_NEW_API_URL_LIVE = "http://indianshippers.com/webapi/";
    readonly YOUR_API_ABOUT = "http://indianshippers.com/about-us";
   readonly YOUR_API_RATE_CARD = "http://indianshippers.com/rate-card";
    
    readonly GET_CATEGORIES = this.YOUR_NEW_API_URL_LIVE+"dashboard/getcategory";
  readonly CHECK_REGISTER = this.YOUR_NEW_API_URL_LIVE+"register";
   readonly RE_SEND = this.YOUR_NEW_API_URL_LIVE+"register/resendotp";
   readonly RE_SEND_OTP = this.YOUR_NEW_API_URL_LIVE+"register/resendotpforforgot";
   readonly UPDATE_PASSWORD = this.YOUR_NEW_API_URL_LIVE+"register/forgotupdatepassword";
   readonly LOGIN = this.YOUR_NEW_API_URL_LIVE+"register/loginwithpassword";
   readonly CHANGE_PASSWORD = this.YOUR_NEW_API_URL_LIVE+"register/changepassword";
   readonly OTP_VERIFY_FOR_REGISTER = this.YOUR_NEW_API_URL_LIVE+"register/verifyotp";
   readonly facebook = this.YOUR_NEW_API_URL_LIVE+"register/facebooklogincheck";
   readonly facebook_login = this.YOUR_NEW_API_URL_LIVE+"register/facebooklogin";
   readonly google = this.YOUR_NEW_API_URL_LIVE+"register/googlepluslogincheck";
   readonly google_login = this.YOUR_NEW_API_URL_LIVE+"register/facebooklogin";
   readonly FINAL_REGISTER = this.YOUR_NEW_API_URL_LIVE+"register/finalregis";
   readonly getDashboard = this.YOUR_NEW_API_URL_LIVE+"dashboard/newindex";
   readonly getDriverLatLong = this.YOUR_NEW_API_URL_LIVE+"dashboard/getdriverlatlong";
   readonly getShareScreen = this.YOUR_NEW_API_URL_LIVE+"register/sharetext";
   readonly getWalletHistory = this.YOUR_NEW_API_URL_LIVE+"dashboard/userwallethistorynew";
   readonly goodscategory = this.YOUR_NEW_API_URL_LIVE+"booking/goodscategory";
   readonly continueBook = this.YOUR_NEW_API_URL_LIVE+"booking/confirmbooking";
   readonly findDriver = this.YOUR_NEW_API_URL_LIVE+"dashboard/currentbookingdetail";
   readonly cancelRequest = this.YOUR_NEW_API_URL_LIVE+"booking/cancelbooking";
   readonly bookingDetail = this.YOUR_NEW_API_URL_LIVE+"dashboard/currentbookingdetail";
   readonly bookingPayment = this.YOUR_NEW_API_URL_LIVE+"orders/paymentresponserazorpay";
   readonly changePaymentMethod = this.YOUR_NEW_API_URL_LIVE+"booking/changePaymentMethod";
   readonly makeayment = this.YOUR_NEW_API_URL_LIVE+"booking/changePaymentMethod";
   readonly rateDriver = this.YOUR_NEW_API_URL_LIVE+"booking/ratedriver";
   readonly getNotification = this.YOUR_NEW_API_URL_LIVE+"faq/notification";
   readonly getOffers = this.YOUR_NEW_API_URL_LIVE+"dashboard/offerce";
   readonly getFaq = this.YOUR_NEW_API_URL_LIVE+"faq";
   readonly getUserBookingList = this.YOUR_NEW_API_URL_LIVE+"booking/userbookinglist";
   readonly getInviteEarn = this.YOUR_NEW_API_URL_LIVE+"dashboard/drivernotification";
   readonly SLIDER = this.YOUR_NEW_API_URL_LIVE+"driver-registration/slider";
   readonly PROFILE_UPDATE = this.YOUR_NEW_API_URL_LIVE+"driver-registration/updateuserprofile";
   readonly FIRST_REGISTRATION = this.YOUR_NEW_API_URL_LIVE+"driver-registration/index";
   readonly OTP_VERIFICATION = this.YOUR_NEW_API_URL_LIVE+"driver-registration/verifyotp";
   readonly DRIVER_IMAGE_PATH =  "http://indianshippers.com/webroot/img/uploads/driver_image/";
   readonly CATEGORY_IMAGE_PATH =  "http://indianshippers.com/webroot/img/uploads/category_icon/";
	
   
  constructor(public http: Http, public loader: LoadingController, public storage: Storage, public actionSheetCtrl: ActionSheetController,public alertCtrl:AlertController,private toastCtrl:ToastController ) {
    console.log('Hello ServicesProvider Provider');
  }


public _loader_content() {
    let loading = this.loader.create({
      spinner: 'hide',
      content: '<img src="../../assets/icon/truck.gif" class="loader" >',
      duration: 10000000
    });
    return loading;
  }
  simplealert(title,message)
  {
	  
	   let alert = this.alertCtrl.create({
    title: title,
    message: message,
    buttons: ['ok']
  });
  alert.present();
  }

  presentToast(strmessage) {
  let toast = this.toastCtrl.create({
    message: strmessage,
    duration: 1000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}
ValidateEmail(mail) 
{
	let regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
 if (regExp.test(mail))
  {
    return true;
  }else{
	  return false;
  }
}
isValidMobile(mobilenumber){

    let regExp = /^[0-9]{10}$/;
  
    if (regExp.test(mobilenumber)) {
		 if (mobilenumber.length==10) {
        return  true;
		 }else{
			 return  false;	 
		 }
		}else{
	   return  false;	
	}
 }

 public log(error: any, from_page: any) {
    console.log(error, from_page);
  }
 
private _handleError(error: any): ErrorObservable {
    return Observable.throw(error);
}
private _extractData(response: Response) {
    if (response.status < 200 || response.status >= 300) {
        throw new Error('Bad response status: ' + response.status);
    }
    //console.log("response:: ", response._body);
    let json_response: any = {};
    let resBody: any = response.text();
    /*console.log("response:: body::", resBody);*/
    if (resBody != '') {
        json_response = response.json();
    }
    return json_response;
}


/******************************DEVELOPE BY MANISH GARG********************/
onCheckRegister(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.CHECK_REGISTER, data, options).map(this._extractData).catch(this._handleError);
  }
onLoginPassword(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.LOGIN, data, options).map(this._extractData).catch(this._handleError);
}  
onResendOtp(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.RE_SEND, data, options).map(this._extractData).catch(this._handleError);
} 
onVerifyRegisterOtp(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.OTP_VERIFY_FOR_REGISTER, data, options).map(this._extractData).catch(this._handleError);
} 

onFinalRegistration(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.FINAL_REGISTER, data, options).map(this._extractData).catch(this._handleError);
} 
updatepassword(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.UPDATE_PASSWORD, data, options).map(this._extractData).catch(this._handleError);
} 
getwallethistorynew(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getWalletHistory, data, options).map(this._extractData).catch(this._handleError);
} 
gettriphistory(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getUserBookingList, data, options).map(this._extractData).catch(this._handleError);
}
 getShareScreendata(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getShareScreen, data, options).map(this._extractData).catch(this._handleError);
} 
 getfaq(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getFaq, data, options).map(this._extractData).catch(this._handleError);
} 
updateprofile(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.PROFILE_UPDATE, data, options).map(this._extractData).catch(this._handleError);
} 
getcategorydata(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.GET_CATEGORIES, data, options).map(this._extractData).catch(this._handleError);
} 
getgoodscategory(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.goodscategory, data, options).map(this._extractData).catch(this._handleError);
}
 confirmbooking(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.continueBook, data, options).map(this._extractData).catch(this._handleError);
} 
getoffercedata(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getOffers, data, options).map(this._extractData).catch(this._handleError);
}
getDashboardData(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.getDashboard, data, options).map(this._extractData).catch(this._handleError);
} 
cancelbookingrequest(data:any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.cancelRequest, data, options).map(this._extractData).catch(this._handleError);
} 

/******************************DEVELOPE BY MANISH GARG********************/
saveuser(credentials) {
    this.storage.set('user_data', credentials);
    return 0;
}
saveuserlocation(credentials) {
    this.storage.set('user_data_location', credentials);
    return 0;
}
getuser() {
    return new Promise(resolve => {
      this.storage.get('user_data').then(data => {
        resolve(data);
      });
    });
}
saverecentsearch(credentials) {
    this.storage.set('recent_search', credentials);
    return 0;
}
getuserrecentlocation() {
    return new Promise(resolve => {
      this.storage.get('recent_search').then(data => {
        resolve(data);
      });
    });
}
 
getuserlocation() {
    return new Promise(resolve => {
      this.storage.get('user_data_location').then(data => {
        resolve(data);
      });
    });
}
  getfirstChar(str)
  {
  return str.charAt(0);
  // return 'a';
  }
  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        return d;
    }

      deg2rad(deg) {
      return deg * (Math.PI/180)
    }
gettimecalculate(distance)
{
	//  time = distance/speed;
	let time:any;
	time = distance/20;
	time = time*60;
	return parseInt(time);
}

getmintimeavaildriver(driverarray)
{
	let mintime:any=0;
	driverarray.forEach((Item)=>{
		if(mintime==0 || Item.timed<mintime)
		{
			mintime =Item.timed; 
			
		}
		
	});
	return mintime;
}

getdriveridbycomma(driver_online_list)
{
	let ids:any=[];
	driver_online_list.forEach((key)=>{
		ids.push(parseInt(key.driverId));
	});
	return ids.join(',');
}
getpriceondistance(catdata,distance)
{
	let intEstimatedCharge =0; 
 if(distance>5)
{
    intEstimatedCharge =  catdata.category_per_km*(distance-5);
 }else{
    
   intEstimatedCharge =  0;
}
let datas = intEstimatedCharge+catdata.category_base_fair;
return datas.toFixed(2);
}
}
